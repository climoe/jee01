package com.craftincode.ekj.advanced.hellojee.customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/customers")
public class CustomerServlet extends HttpServlet {

    private List<Customer> customers = new ArrayList<>();

    @Override
    public void init() throws ServletException {
        customers.add(new Customer(1, "Adam", "Kowalski", 1980));
        customers.add(new Customer(2, "Jan", "Kowalski", 1950));
        customers.add(new Customer(3, "Jan", "Nowak", 1988));
        customers.add(new Customer(4, "Adaś", "Miauczyński", 1990));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter respWriter = resp.getWriter();

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");

        for (Customer customer : customers) {
            if (id != null && !Integer.toString(customer.getId()).equals(id)) {
                continue;
            }
            if (name != null && !customer.getName().equals(name)) {
                continue;
            }
            if (surname != null && !customer.getSurname().equals(surname)) {
                continue;
            }

            respWriter.append(customer.toString());
        }

        // EW z użyciem JAVA 8
//        customers.stream()
//                .filter(c -> matches(c, id, name, surname))
//                .forEach(c -> respWriter.append(c.toString()));

        respWriter.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String birthYear = req.getParameter("birthYear");

        Customer customer = new Customer(Integer.parseInt(id), name, surname, Integer.parseInt(birthYear));

        customers.add(customer);

        resp.setContentType("text/html");
        resp.getWriter().append("Stworzono klienta!!!").flush();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");

        for (Customer customer : customers) {
            if (Integer.toString(customer.getId()).equals(id)) {
                customers.remove(customer);
                break;
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String newName = req.getParameter("name");
        String newSurname = req.getParameter("surname");
        String newBirthYear = req.getParameter("birthYear");

        // pętla wyszukująca klienta o podanym ID
        for (Customer customer : customers) {
            if (Integer.toString(customer.getId()).equals(id)) {
                // aktualizacja danych klienta
                if (newName != null) {
                    customer.setName(newName);
                }

                if (newSurname != null) {
                    customer.setSurname(newSurname);
                }

                if (newBirthYear != null) {
                    customer.setBirthYear(Integer.parseInt(newBirthYear));
                }

                break;
            }
        }

        // WERSJA JAVA 8
        customers.stream()
                .filter(c -> Integer.toString(c.getId()).equals(id))
                .forEach(c -> {
                    if (newName != null) {
                        c.setName(newName);
                    }

                    if (newSurname != null) {
                        c.setSurname(newSurname);
                    }

                    if (newBirthYear != null) {
                        c.setBirthYear(Integer.parseInt(newBirthYear));
                    }
                });

    }


    private boolean matches(Customer customer, String id, String name, String surname) {
        if (id != null && !Integer.toString(customer.getId()).equals(id)) {
            return false;
        }
        if (name != null && !customer.getName().equals(name)) {
            return false;
        }
        if (surname != null && !customer.getSurname().equals(surname)) {
            return false;
        }

        return true;
    }
}
