package com.craftincode.ekj.advanced.hellojee;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "myFirstServlet1", urlPatterns = "/abc")
public class MyFirstServlet extends HttpServlet {

    // metoda wykonywana gdy tworzony jest obiekt servletu
    @Override
    public void init() throws ServletException {
        System.out.println("Tworzę servlet");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        writer.append("Hello World!");
        writer.flush();
    }
}
