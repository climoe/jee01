package com.craftincode.ekj.advanced.hellojee;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/router")
public class RouterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String site = request.getParameter("site");

        switch (site) {
            case "contact":
                response.sendRedirect(response.encodeRedirectURL("contact.html"));
                break;
            case "about":
                response.sendRedirect(response.encodeRedirectURL("about.html"));
                break;
            case "main":
                response.sendRedirect(response.encodeRedirectURL("main.html"));
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Nie ma takiej strony!!!");

        }
    }
}
